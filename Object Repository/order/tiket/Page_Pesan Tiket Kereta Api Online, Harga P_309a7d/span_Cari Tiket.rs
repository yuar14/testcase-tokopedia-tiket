<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Cari Tiket</name>
   <tag></tag>
   <elementGuidId>3cc9a4f5-ea4f-4df6-941a-b5dcfaeda541</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.button.css-d41a3e-unf-btn.eg8apji0 > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div/div/div/div/div/div[2]/section/div[2]/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7c774c7d-3c4c-4509-b83a-d9d91edb5ae3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cari Tiket</value>
      <webElementGuid>8e9f429d-c3a1-4197-a689-a6a4a07ca019</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;css-14u6y52&quot;]/div[@class=&quot;css-8atqhb e1iwxiko0&quot;]/div[@class=&quot;css-17uduf3&quot;]/div[@class=&quot;css-nsmt50&quot;]/div[@class=&quot;css-lbroap&quot;]/div[@class=&quot;css-1m9l7hb&quot;]/section[@class=&quot;css-q1vfvh-unf-card eeeacht0&quot;]/div[@class=&quot;css-wkfdkq&quot;]/button[@class=&quot;button css-d41a3e-unf-btn eg8apji0&quot;]/span[1]</value>
      <webElementGuid>0f9b0b0b-3c58-4a6f-8aad-e00e0e5d732d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/div/div/div/div/div[2]/section/div[2]/button/span</value>
      <webElementGuid>2795cfd1-062c-4d9a-a625-aa0e10af0f7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jumlah Penumpang'])[1]/following::span[1]</value>
      <webElementGuid>6738147a-61f1-4135-8dca-1155f22494bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pulang'])[1]/following::span[1]</value>
      <webElementGuid>c506ac4c-d807-460c-ac79-bb148ed418ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='STASIUN POPULER'])[1]/preceding::span[1]</value>
      <webElementGuid>1ef29825-70a4-4b4a-9433-30d5847e3140</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bandung - Bandung (BD)'])[1]/preceding::span[1]</value>
      <webElementGuid>c841435a-7a93-4b33-8a90-524c941af078</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Cari Tiket']/parent::*</value>
      <webElementGuid>620451a8-b433-484e-a5be-29a7374b0bc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button/span</value>
      <webElementGuid>507e99a2-a99c-4efa-88ee-f9dd07a645ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Cari Tiket' or . = 'Cari Tiket')]</value>
      <webElementGuid>5a1c5650-1fc3-4f30-bef4-ff2e6f8f607a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
