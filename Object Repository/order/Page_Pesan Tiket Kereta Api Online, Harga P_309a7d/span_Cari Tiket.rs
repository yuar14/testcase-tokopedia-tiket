<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Cari Tiket</name>
   <tag></tag>
   <elementGuidId>c44ca674-00e2-4e88-907e-72a39ca2e1d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.button.css-d41a3e-unf-btn.eg8apji0 > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div/div/div/div/div/div[2]/section/div[2]/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>17ec3c19-17cc-47b8-aefb-0c36ed39c861</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cari Tiket</value>
      <webElementGuid>ff24c8d8-9d9a-4f7a-9f19-ca0c9dc496cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;css-14u6y52&quot;]/div[@class=&quot;css-8atqhb e1iwxiko0&quot;]/div[@class=&quot;css-17uduf3&quot;]/div[@class=&quot;css-nsmt50&quot;]/div[@class=&quot;css-lbroap&quot;]/div[@class=&quot;css-1m9l7hb&quot;]/section[@class=&quot;css-q1vfvh-unf-card eeeacht0&quot;]/div[@class=&quot;css-wkfdkq&quot;]/button[@class=&quot;button css-d41a3e-unf-btn eg8apji0&quot;]/span[1]</value>
      <webElementGuid>ae5cec29-37af-43a4-8ceb-98faae44f1f4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/div/div/div/div/div[2]/section/div[2]/button/span</value>
      <webElementGuid>a8611c03-f177-44a7-82d9-22833b47cd1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jumlah Penumpang'])[1]/following::span[1]</value>
      <webElementGuid>bb87e25b-95cf-4e93-ad3a-aac61dcdd9dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pulang'])[1]/following::span[1]</value>
      <webElementGuid>30689982-ac0d-474d-9a75-4d8fed0820c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='STASIUN POPULER'])[1]/preceding::span[1]</value>
      <webElementGuid>d370202c-75ce-4532-b9c3-75d476d919f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bandung - Bandung (BD)'])[1]/preceding::span[1]</value>
      <webElementGuid>74baeb72-36a6-46e0-aa4f-ed4fe933346c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Cari Tiket']/parent::*</value>
      <webElementGuid>bd445568-8f82-4b51-a611-9f2ee6e86838</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button/span</value>
      <webElementGuid>c7c7b2bb-d87e-4e4c-bba6-53d36df958f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Cari Tiket' or . = 'Cari Tiket')]</value>
      <webElementGuid>b4a91b7d-1348-4cb6-9ab3-2e717ab7dc82</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
