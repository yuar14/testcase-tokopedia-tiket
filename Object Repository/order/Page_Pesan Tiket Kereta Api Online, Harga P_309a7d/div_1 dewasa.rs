<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_1 dewasa</name>
   <tag></tag>
   <elementGuidId>a4952379-fc6f-4383-a1e4-ec0059a61893</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.passengers > div.css-k008qs > div.css-22hwsp</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div/div/div/div/div/div[2]/section/div/div[3]/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>235f3f7b-3ff6-424a-a327-ac42ca44df74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-22hwsp</value>
      <webElementGuid>1242fdef-21e7-4c93-a7e7-5bfb47adb477</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>selectorJumlah Penumpang</value>
      <webElementGuid>f4a0c5dd-ea9f-428c-9317-fd769cbb8f68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>1 dewasa</value>
      <webElementGuid>9543f4b4-09bd-4940-9c25-b7135d954986</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;css-14u6y52&quot;]/div[@class=&quot;css-8atqhb e1iwxiko0&quot;]/div[@class=&quot;css-17uduf3&quot;]/div[@class=&quot;css-nsmt50&quot;]/div[@class=&quot;css-lbroap&quot;]/div[@class=&quot;css-1m9l7hb&quot;]/section[@class=&quot;css-q1vfvh-unf-card eeeacht0&quot;]/div[@class=&quot;css-16j44w0&quot;]/div[@class=&quot;passengers&quot;]/div[@class=&quot;css-k008qs&quot;]/div[@class=&quot;css-22hwsp&quot;]</value>
      <webElementGuid>efad271f-75d6-4a5a-aad1-66dda8510607</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/div/div/div/div/div[2]/section/div/div[3]/div[2]/div</value>
      <webElementGuid>a848c38e-47e5-4ce5-b5d3-d1800d5ad3ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jumlah Penumpang'])[1]/following::div[2]</value>
      <webElementGuid>c59792d6-da27-453a-8505-3a04cb1f9640</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pulang'])[1]/following::div[7]</value>
      <webElementGuid>2763e106-48b9-4bfb-a8f2-e9c65bd7211b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cari Tiket'])[1]/preceding::div[1]</value>
      <webElementGuid>71b28895-30ca-494b-afc8-3e55a79351ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='STASIUN POPULER'])[1]/preceding::div[2]</value>
      <webElementGuid>3a6fe51e-871c-4447-8de5-0a336881e386</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='1 dewasa']/parent::*</value>
      <webElementGuid>cbaf4e0c-8b3d-4ddf-8e12-0a2efde3a85d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div</value>
      <webElementGuid>dab82654-6758-4216-8591-781ea928ddf5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '1 dewasa' or . = '1 dewasa')]</value>
      <webElementGuid>4a60f2a4-f760-4bfc-b4f1-d2b5b06b87ab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
