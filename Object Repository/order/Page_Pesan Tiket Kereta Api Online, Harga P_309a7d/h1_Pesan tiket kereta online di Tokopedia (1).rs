<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Pesan tiket kereta online di Tokopedia (1)</name>
   <tag></tag>
   <elementGuidId>9ce97bab-ef94-430b-a86c-ababb40b60f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.css-1i82c75</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div/div/div/div/div/div[2]/section/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>67cf4692-1602-4cf2-a9aa-4593a78da20f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-1i82c75</value>
      <webElementGuid>4542849b-8365-4d01-a4e1-c9ee0e0fed11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pesan tiket kereta online di Tokopedia</value>
      <webElementGuid>0d518046-1bf3-4d5f-898d-8efadc843f63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;css-14u6y52&quot;]/div[@class=&quot;css-8atqhb e1iwxiko0&quot;]/div[@class=&quot;css-17uduf3&quot;]/div[@class=&quot;css-nsmt50&quot;]/div[@class=&quot;css-lbroap&quot;]/div[@class=&quot;css-1m9l7hb&quot;]/section[@class=&quot;css-q1vfvh-unf-card eeeacht0&quot;]/h1[@class=&quot;css-1i82c75&quot;]</value>
      <webElementGuid>c69bef3a-41ae-41a3-926e-44b459830f5e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/div/div/div/div/div[2]/section/h1</value>
      <webElementGuid>fa6d1bfb-0d00-4352-a13b-ee7512ae1b6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saat ini tiket kereta sudah dapat dibeli untuk 45 hari kedepan, yuk cek sekarang!'])[1]/following::h1[1]</value>
      <webElementGuid>428b626d-465c-4756-a2c7-630e6f6b7799</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hotel'])[1]/following::h1[1]</value>
      <webElementGuid>c4693173-eaeb-42db-a203-83bba22e95d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Asal'])[1]/preceding::h1[1]</value>
      <webElementGuid>fa4b508a-9719-440b-94b0-d77d3b2f57e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tujuan'])[1]/preceding::h1[1]</value>
      <webElementGuid>435cd513-ce52-43bd-a084-92a2dd2feb94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pesan tiket kereta online di Tokopedia']/parent::*</value>
      <webElementGuid>664a6117-96be-49a7-b1f9-e3bc7cec877d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>49304f53-8686-4ebd-8633-65ff859dde7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Pesan tiket kereta online di Tokopedia' or . = 'Pesan tiket kereta online di Tokopedia')]</value>
      <webElementGuid>31d4fd1a-2be3-48a3-971a-bdbf03a1d1bd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
