<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Asal_css-22hwsp</name>
   <tag></tag>
   <elementGuidId>da50feff-90a8-455f-921e-1fd58316deb8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.css-22hwsp</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='Gambir (GMR)']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>af366a97-1986-4056-b6b3-9d409134d723</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-22hwsp</value>
      <webElementGuid>cca3ecb9-6a3f-4f08-b288-7ebbbb824a16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Gambir (GMR)</value>
      <webElementGuid>e8e2764e-bb50-48c5-9f46-34088f01555a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Gambir (GMR)</value>
      <webElementGuid>60ae237b-742e-4aa7-b713-46be9d6bc913</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>selectorAsal</value>
      <webElementGuid>c6eaabd2-76bd-4a44-aaa2-5bc8a0ac014b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;css-14u6y52&quot;]/div[@class=&quot;css-8atqhb e1iwxiko0&quot;]/div[@class=&quot;css-17uduf3&quot;]/div[@class=&quot;css-nsmt50&quot;]/div[@class=&quot;css-lbroap&quot;]/div[@class=&quot;css-1m9l7hb&quot;]/section[@class=&quot;css-q1vfvh-unf-card eeeacht0&quot;]/div[@class=&quot;css-16j44w0&quot;]/div[@class=&quot;location&quot;]/div[@class=&quot;stretch&quot;]/input[@class=&quot;css-22hwsp&quot;]</value>
      <webElementGuid>41dc7ae7-9ee6-4a5b-8a98-a08aa7571abc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='Gambir (GMR)']</value>
      <webElementGuid>4b47524c-4d78-4743-a900-ec098730dfb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/div/div/div/div/div[2]/section/div/div/div/input</value>
      <webElementGuid>ce1b1b79-9728-4cf9-b24d-5811a9060d23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>715d1cc0-5d66-45cb-a57d-39be4d049134</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Gambir (GMR)']</value>
      <webElementGuid>ee4bbcd6-81de-44d4-9daf-b99a4b142705</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
