<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Travel  Entertainment</name>
   <tag></tag>
   <elementGuidId>5d8a536f-d122-4491-8a9e-c6984ecf5dbb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='header-main-wrapper']/div[2]/div/div/div[2]/div/div/div[7]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>eb87f01d-149a-480d-9f3c-e5fef11312e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Travel &amp; Entertainment</value>
      <webElementGuid>c4c026d9-36fb-4568-b60d-a69073b4a2d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;header-main-wrapper&quot;)/div[@class=&quot;css-12jp264 e90swyx2&quot;]/div[@class=&quot;css-c4s9dq e1429ojz0&quot;]/div[@class=&quot;css-1551isn e1429ojz1&quot;]/div[@class=&quot;css-2g3zpp e1429ojz2&quot;]/div[@class=&quot;css-11womkr-unf-tab e1429ojz3&quot;]/div[@class=&quot;css-1sj9ctn&quot;]/div[@class=&quot;css-11icpy4&quot;]/div[1]</value>
      <webElementGuid>1a89fe9c-fcd8-45bf-83da-6b4412af31bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='header-main-wrapper']/div[2]/div/div/div[2]/div/div/div[7]/div</value>
      <webElementGuid>1a362c4e-0a2b-4549-8c97-d0ef6ab21abf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pajak &amp; Pendidikan'])[1]/following::div[2]</value>
      <webElementGuid>7286130d-8f3f-4db7-a3a5-66f3195fb331</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Investasi Asuransi &amp; Pinjaman'])[1]/following::div[4]</value>
      <webElementGuid>a77412d4-f506-47cb-bca2-fa9347a219bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Halal Corner'])[1]/preceding::div[1]</value>
      <webElementGuid>7bd12d24-4c06-4560-95e7-d2705c9f93dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Brand Unggulan'])[1]/preceding::div[3]</value>
      <webElementGuid>09de2bff-ef89-4b53-976b-25dd0d05748d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Travel &amp; Entertainment']/parent::*</value>
      <webElementGuid>053e1892-907c-4885-abe5-82777870bb3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div</value>
      <webElementGuid>4b1bf022-bebf-4dfa-9494-e5713f3a00d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Travel &amp; Entertainment' or . = 'Travel &amp; Entertainment')]</value>
      <webElementGuid>d2f5d606-d345-4376-8350-d72af9ca77ec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
