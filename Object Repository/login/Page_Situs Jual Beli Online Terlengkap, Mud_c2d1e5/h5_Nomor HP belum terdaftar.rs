<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Nomor HP belum terdaftar</name>
   <tag></tag>
   <elementGuidId>1ba57db5-f6ac-4448-a68b-e6ec64a830b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h5.css-xvn0u1-unf-heading.e1qvo2ff5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Hubungi Tokopedia Care'])[1]/following::h5[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>509973bd-8594-414c-b844-2505f1f79259</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-unify</name>
      <type>Main</type>
      <value>Typography</value>
      <webElementGuid>62aabd95-be60-4a36-b84a-6af640fa8e92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-xvn0u1-unf-heading e1qvo2ff5</value>
      <webElementGuid>67e750de-25ae-4a8a-844c-03d8be618af5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Nomor HP belum terdaftar</value>
      <webElementGuid>6777a294-44af-46e0-8b1b-28fdee534af2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;css-1g5w2rw&quot;]/div[9]/div[@class=&quot;css-1gau7m8-unf-dialog ef541p40&quot;]/h5[@class=&quot;css-xvn0u1-unf-heading e1qvo2ff5&quot;]</value>
      <webElementGuid>94da4d19-2e98-4de4-8cbb-6ab18d89e81f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hubungi Tokopedia Care'])[1]/following::h5[1]</value>
      <webElementGuid>57a451f8-f334-415b-a0f1-0c07c7ce25c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Butuh bantuan?'])[1]/following::h5[1]</value>
      <webElementGuid>c3b16873-429f-490c-afd1-0c311f240bc9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ya, Daftar'])[1]/preceding::h5[1]</value>
      <webElementGuid>fb44dadf-0d52-48cb-a498-723b40c15c5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Nomor HP belum terdaftar']/parent::*</value>
      <webElementGuid>b9a328f3-4be5-4e39-9a99-84941ab21f97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h5</value>
      <webElementGuid>89aa5ec7-1412-4fb8-b110-5d8d70f645ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Nomor HP belum terdaftar' or . = 'Nomor HP belum terdaftar')]</value>
      <webElementGuid>c30a836a-97dd-46b9-bea8-279b089cb84b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
