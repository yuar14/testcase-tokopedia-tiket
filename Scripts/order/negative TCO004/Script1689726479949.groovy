import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.tokopedia.com/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/order/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/div_Kategori'))

WebUI.click(findTestObject('Object Repository/order/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/div_Travel  Entertainment'))

WebUI.click(findTestObject('Object Repository/order/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/a_Tiket Kereta Api'))

WebUI.verifyElementText(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/h1_Pesan tiket kereta online di Tokopedia (1)'), 
    'Pesan tiket kereta online di Tokopedia')

WebUI.click(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/input_Asal_css-22hwsp'))

WebUI.click(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/div_Bandung - Bandung (BD)'))

WebUI.click(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/input_Tujuan_css-22hwsp'))

WebUI.click(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/div_Cirebon - Cirebon (CN)'))

WebUI.click(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/div_20 Jul 2023'))

WebUI.click(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/div_18'))

WebUI.click(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/div_1 dewasa'))

WebUI.click(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/div_1 dewasa'))

WebUI.click(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/span_Simpan'))

WebUI.click(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/span_Cari Tiket'))

WebUI.verifyElementText(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/div_Jum, 18 Agu 2023  1 Dewasa'), 
    'Jum, 18 Agu 2023 | 1 Dewasa')

WebUI.click(findTestObject('Object Repository/order/pesanan/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/span_Pilih'))

WebUI.navigateToUrl('https://www.tokopedia.com/login?ld=%2Fkereta-api%2Fatc%2F%3Fid%3DeyJ0cmFpbiI6eyJqb3VybmV5cyI6W3sic2NoZWR1bGVJRCI6IjY2NTg5NDE0NiIsIm9yaWdpbiI6IkJEIiwiZGVzdGluYXRpb24iOiJDTiIsImRlcGFydHVyZURhdGUiOiIyMDIzMDgxOCJ9XSwiYWR1bHQiOjEsImNoaWxkIjowLCJpbmZhbnQiOjB9LCJzZWFyY2hUZXJtIjoiP2E9MSZkPTIwMjMwODE4Jmk9MCZyPUJELkNOIn0%3D')

WebUI.closeBrowser()

