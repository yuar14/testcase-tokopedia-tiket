import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.tokopedia.com/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/order/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/div_Kategori'))

WebUI.click(findTestObject('Object Repository/order/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/div_Travel  Entertainment'))

WebUI.click(findTestObject('Object Repository/order/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/a_Tiket Kereta Api'))

WebUI.verifyElementText(findTestObject('Object Repository/order/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/h1_Pesan tiket kereta online di Tokopedia (1)'), 
    'Pesan tiket kereta online di Tokopedia')

WebUI.click(findTestObject('Object Repository/order/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/input_Asal_css-22hwsp'))

WebUI.click(findTestObject('Object Repository/order/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/div_Bandung - Bandung (BD)'))

WebUI.verifyElementText(findTestObject('Object Repository/order/Page_Pesan Tiket Kereta Api Online, Harga P_309a7d/div_Stasiun keberangkatan dan tujuan tidak _b32c38'), 
    'Stasiun keberangkatan dan tujuan tidak boleh sama.')

WebUI.closeBrowser()

